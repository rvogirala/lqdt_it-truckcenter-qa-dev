package lqdt.tc.pages;

import java.util.Calendar;
import java.util.GregorianCalendar;

import lqdt.tc.common.PageBase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RegisterPage extends PageBase {
	
	
	public RegisterPage(WebDriver driver) {
		super(driver);//always include this in any page class
	
	}

	//Truck Center New User Registration Page Elements & Method
	
	private WebElement firstName;
	private WebElement lastName;
	private WebElement email;
	private WebElement email2;
	
	private WebElement companyName;
	private WebElement address1;
	private WebElement address2;
	private WebElement city;
	private WebElement state;
	
	private WebElement countryCode;
	private WebElement postalCode;
	private WebElement phoneNumber;
	private WebElement username;
	private WebElement password;
	
	private WebElement password2;
	private WebElement busType;
	private WebElement registerbutton;
	
	public void NewUserRegistration( ) { 
		
		String UniqueValue, lastnameval, emailval; 
		String s = "LN";
		String conct1 = null;	
//		UniqueValue=getFromattedUsername(conct1);
	//	lastnameval=s+UniqueValue;
		//emailval =UniqueValue+"@gmail.com";
		firstName.sendKeys("AutoUser");
//    	lastName.sendKeys(lastnameval);
//		email.sendKeys(emailval);
//		email2.sendKeys(emailval);
		
		companyName.sendKeys("LSI TC");
		address1.sendKeys("3000 Internet Blvd");
		address2.sendKeys("Suite #109");
		city.sendKeys("Frisco");
		state.sendKeys("TX");
		
		countryCode.sendKeys("USA");
		postalCode.sendKeys("75034");
		phoneNumber.sendKeys("202-467-6868");
//		username.sendKeys("TC"+UniqueValue);
		password.sendKeys("Take11easy");
		password2.sendKeys("Take11easy");
		busType.sendKeys("BU");
		registerbutton.click();
		
		
	 }
	


		
}//End Class
