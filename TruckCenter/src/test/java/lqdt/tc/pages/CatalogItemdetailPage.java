package lqdt.tc.pages;

import lqdt.tc.common.PageBase;
import lqdt.tc.common.TestBase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CatalogItemdetailPage extends PageBase {

	public CatalogItemdetailPage(WebDriver driver) {
		super(driver);//always include this in any page class
	
	}
		

	private WebElement name;
	private WebElement phone;
	private WebElement email;
	private WebElement offer;
	
	@FindBy(how=How.XPATH, using = "//*[@id='placebid']/ul/li[6]/table/tbody/tr/td[2]/input")
	private WebElement submitoffer;
	
	@FindBy(how=How.XPATH, using = "//*[@id='buyNowMessage']")
	private WebElement confirmedmessage; // Your offer of $10000.00 has been submitted.
	
	public void SubmitOffer() { 
		String emailval , UniqueValue, conct1; 
		conct1 = null;
//		UniqueValue=getFromattedUsername(conct1);
//		emailval =UniqueValue+"@gmail.com";
		name.sendKeys("AutoUser Testing");
		phone.sendKeys("202-467-6868 ");
//		email.sendKeys(emailval);
		offer.sendKeys("10000.00");
		submitoffer.click();
	 }
	
}//End Class
