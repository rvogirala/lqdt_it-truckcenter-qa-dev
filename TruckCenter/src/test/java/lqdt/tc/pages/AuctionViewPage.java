package lqdt.tc.pages;

import lqdt.tc.common.PageBase;
import lqdt.tc.common.TestBase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuctionViewPage extends PageBase {

	public AuctionViewPage(WebDriver driver) {
		super(driver);//always include this in any page class
	
	}
		

	@FindBy(how=How.XPATH, using = "/*[@id='bidAmount']")
	private WebElement yourbid;
	
	private WebElement placebidbutton;
	
	@FindBy(how=How.ID, using ="//*[@id='placebid']/ul/li[2]" )
	private WebElement Lowestbid; // to get Lowest you may Bid $2,320.00: 
	
	public void MakeanOffer() { 
		String auctionValue = driver.findElement(By.xpath("//*[@id='placebid']/ul/li[2]")).getText();
		String[] Temp1;
        String[] Temp2;
        String Bidval = null;
        String Splitval = null;
        if (auctionValue != null) {
        	Temp1 = auctionValue.split("\\$", 0);     
            Temp2 = Temp1[1].split("\\.", 0);
            Bidval = Temp2[0];
            Splitval = Bidval.replaceAll(",","");

        }

		yourbid.sendKeys(Splitval);
		placebidbutton.click();

	 }
	
	
}//End Class
