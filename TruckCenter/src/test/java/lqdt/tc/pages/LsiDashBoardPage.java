package lqdt.tc.pages;

import lqdt.tc.common.PageBase;
import lqdt.tc.common.TestBase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LsiDashBoardPage extends PageBase {

	public LsiDashBoardPage(WebDriver driver) {
		super(driver);//always include this in any page class
	
	}

	
	//Truck Center Home Page Elements & Method
//	<<<<< Top Navigation upper section>>>>>>>
			
	private WebElement user_menuitem;
	@FindBy(how=How.XPATH, using = "//*[@id='user_menuitem']/a")
	private WebElement v3Users;
	private WebElement username;
	private WebElement submit_search_user;
	@FindBy(how=How.XPATH, using = "//*[@id='user_row_1']/td[2]/a")
	private WebElement useridrow;
	private WebElement platform_id;
	@FindBy(how=How.XPATH, using = "//th[@class='header' and text()='Status:']")
	private WebElement status;
	private WebElement save_button;
	private String statusValue = null;
	
	public void LsiUserActivation() { 
		String lsiusername = null;
				
		user_menuitem.click();
		v3Users.click();
		username.sendKeys(System.getProperty(lsiusername));
		platform_id.click();
		platform_id.sendKeys("Truckcenter");
		submit_search_user.click();
		WebElement checkBox = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("check_all_checkbox")));
		driver.switchTo().frame("iframe_User");
		useridrow.click();
        driver.switchTo().defaultContent();
        driver.switchTo().frame("detail_users");
        WebElement detailUserTable  = new WebDriverWait(driver, 30)
        	.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//form/table[1]/tbody")));
        status.click();
        status.sendKeys("Active");
        save_button.click();
        statusValue = status.getText();
        if(statusValue == "Active")
        {
        	System.out.println("User is activated");
        }else
        {
        	System.out.println("User is not activated");
        }
		
	 }
	
	
	
}//End Class
