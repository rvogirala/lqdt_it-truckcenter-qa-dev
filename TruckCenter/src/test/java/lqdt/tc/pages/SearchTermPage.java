package lqdt.tc.pages;

import lqdt.tc.common.PageBase;
import lqdt.tc.common.TestBase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchTermPage extends PageBase {

	public SearchTermPage(WebDriver driver) {
		super(driver);//always include this in any page class
	
	}
		

	@FindBy(how=How.XPATH, using = "//*[@id='addtowatchlist_paging']/input")
	private WebElement addAlllotstowatchlist;
	
	@FindBy(how=How.XPATH, using = "//*[@id='auctionlist']/tbody/tr[2]/td[1]/a/div/img")
	private WebElement firstAuctionItem;
	
	@FindBy(how=How.ID, using ="//*[@id='displaying']/label[1]" )
	private WebElement Displaying; // to check Displaying 1-10 
	
	public void SelectfirstAuction() { 
		addAlllotstowatchlist.click();
	 }
	
	public void AddtoWatchlist() { 
		firstAuctionItem.click();
	 }
}//End Class
