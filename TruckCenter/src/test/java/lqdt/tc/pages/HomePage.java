package lqdt.tc.pages;

import lqdt.tc.common.PageBase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HomePage extends PageBase {

	public HomePage(WebDriver driver) {
		super(driver);//always include this in any page class
	
	}

	
	//Truck Center Home Page Elements & Method
//	<<<<< Top Navigation upper section>>>>>>>
			
	private WebElement login;
	
	public void  LoginTruckCenter() { 
		 login.click();
	 }
	
	private WebElement register;
	
	public void NewuserRegister() { 
		 register.click();
	 }
	
	@FindBy(how=How.LINK_TEXT, using = "My Truck Center")
	private WebElement myTruckCenterLink;

	public void MyTruckcenter() { 
		myTruckCenterLink.click();
	 }
	@FindBy(how=How.XPATH, using = "//*[@id='channel']/option[0]")
	private WebElement retail;
	@FindBy(how=How.XPATH, using = "//*[@id='channel']/option[1]")
	private WebElement liveAuction;
	@FindBy(how=How.XPATH, using = "//*[@id='channel']/option[2]")
	private WebElement onlineAuction; 
	
	private WebElement keywordsearch;
	private WebElement gosearch;
	
	public void RegularSearch(String strRegularSearch) { 
		// Add code drop down handle<<<<<<<<<<<<<< check this>>>>>>>>>>>>>>>>>
		keywordsearch.sendKeys(strRegularSearch);
		gosearch.click();
		 }
	
	@FindBy(how=How.LINK_TEXT, using = "Help")
	private WebElement helpLink;
	public void HelpLink() { 
		helpLink.click();
	 }
	
	private WebElement TruckCenterLogo;
	
	public void Logo() { 
		TruckCenterLogo.click();
	 }
//	<<<<< Top Navigation lower  section>>>>>>>
	@FindBy(how=How.LINK_TEXT, using = "HOME")
	private WebElement homeLink;
	
	public void HomeLink() { 
		helpLink.click();
	 }
	
	@FindBy(how=How.LINK_TEXT, using = "LOCATIONS")
	private WebElement locationLink;
	
	public void LocationLink() { 
		locationLink.click();
	 }
	
	@FindBy(how=How.LINK_TEXT, using = "ABOUT US")
	private WebElement aboutusLink;
	
	public void AboutusLink() { 
		aboutusLink.click();
	 }
	
//	<<<<< Left Navigation lower  section>>>>>>>	
	
	@FindBy(how=How.LINK_TEXT, using = "We can help you sell your trucks")
	private WebElement sellyourTruck;
	
	public void LeftBannerTruckLink() { 
		sellyourTruck.click();
	 }
	
	@FindBy(how=How.LINK_TEXT, using = "Sign up for FREE email alerts")
	private WebElement emailAlerts;
	
	public void LeftBannerEmailLink() { 
		emailAlerts.click();
	 }
	
	@FindBy(how=How.LINK_TEXT, using = "Financing Available. Click here for details.")
	private WebElement financeLink;
	
	public void LeftBannerFinancelLink() { 
		financeLink.click();
	 }
	
//	<<<<< Body of the Home page>>>>>>>	
	
	@FindBy(how=How.LINK_TEXT, using = "View All")
	private WebElement featuredlotviewAll;
	
	public void LotViewAll() { 
		featuredlotviewAll.click();
	 }

	// Advanced Search
	
	@FindBy(how=How.TAG_NAME, using ="'ymm-search" )
	private WebElement advSearch;
	
	public void AdvanceSearch () {
		advSearch.click();
	}
	
//	
//	private WebElement keywordsearch;
//	private WebElement gosearch;
//	
//	public void RegularSearch(String strRegularSearch) { 
//		// Add code drop down handle<<<<<<<<<<<<<< check this>>>>>>>>>>>>>>>>>
//		keywordsearch.sendKeys(strRegularSearch);
//		gosearch.click();
//		 }
	
	
}//End Class
