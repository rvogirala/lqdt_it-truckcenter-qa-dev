package lqdt.tc.pages;

import lqdt.tc.common.PageBase;
import lqdt.tc.common.TestBase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyAccountPage extends PageBase {

	public MyAccountPage(WebDriver driver) {
		super(driver);//always include this in any page class
	
	}
	
	private WebElement selectAll; 
		
	@FindBy(how=How.LINK_TEXT  , using = "Watchlist")
	private WebElement Watchlist;
	
	@FindBy(how=How.ID, using ="//*[@id='bidsfooter']/input[2]" )
	private WebElement RemovefromWatchlist; // to check Displaying 1-10 
	
	public void ViewWatchList() { 
		Watchlist.click();
	 }
	
	public void RemovefromWatchlist() { 
		selectAll.click();
		RemovefromWatchlist.click();
	 }
	
	
}//End Class
