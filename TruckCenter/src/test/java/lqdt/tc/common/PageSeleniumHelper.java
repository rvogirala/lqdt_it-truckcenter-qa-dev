package lqdt.tc.common;



import java.util.Calendar;
import java.util.GregorianCalendar;

import org.openqa.selenium.WebDriver;

public class PageSeleniumHelper {
	
	private WebDriver driver;

	public PageSeleniumHelper(WebDriver driver) {
			this.driver = driver;
	}
	
	public boolean SampleMethod()
	{
		return true;
	}
	
//	protected boolean isElementPresent(By by,WebDriver driver) {
//		try {
//			 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//			return driver.findElement(by).isEnabled();
//		} catch (NoSuchElementException e) {
//			System.out.println("Element is missing "+e);
//			return false;
//		} 
//	}

	// #############################################################################
	// Function Name : getFromattedUsername
	// Description : Function to format username
	// Input Parameters : none
	// Return Value : String
	// Created : Ram vogirala
	// #############################################################################	
	
	public static String getFromattedUsername(String conct1) {
		  int day, month, year;
	      int second, minute, hour;
	      GregorianCalendar date = new GregorianCalendar();
	 
	      String dayU = Integer.toString((int)(date.get(Calendar.DAY_OF_MONTH)));
	      String monthU = Integer.toString((int)(date.get(Calendar.MONTH)+1));
	      String yearU = Integer.toString((int)(date.get(Calendar.YEAR)));
	 
	      String secondU = Integer.toString((int)(date.get(Calendar.SECOND)));
	      String minuteU = Integer.toString((int)(date.get(Calendar.MINUTE)));
	      String hourU = Integer.toString((int)(date.get(Calendar.HOUR)));
	      
	      return conct1 = dayU+monthU+yearU+hourU+minuteU+secondU;
	      
	}
}
