package lqdt.tc.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import lqdt.tc.tests.HomeTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;



@Listeners({WebDriverSuiteListener.class,WebDriverEventListener.class})
public class TestBase {
	

	
	final Logger logger = LoggerFactory.getLogger(HomeTest.class);
	public static WebDriver driver;
	public PageSeleniumHelper helper;
	public Properties prop;
	
	public WebDriver getDriver() {
        return driver;
}
	
	public TestBase() {	}
	
	@BeforeClass
	public  void beforeClass()
	{
		driver = LocalDriverManager.getDriver();//Gets a threadsafe instance of Webdriver
		//Load the properties file
		prop = new Properties();
		
		try {
			prop.load(TestBase.class.getResourceAsStream("/TestData.properties"));
			
		} catch (IOException e) {
			e.printStackTrace();
			logger.info("Unable to load TestData.Properties file");
			System.exit(0);
		}
		catch (NullPointerException e)
		{
			e.printStackTrace();
			logger.info("Unable to load TestData.Properties file. File null.");
			System.exit(0);
			
		}
		
		
	}
	
	public void waitforFrameandswitchtoit(String framename) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(framename));
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
	
	public void main() {};
	
	
	
}//Class


	

 